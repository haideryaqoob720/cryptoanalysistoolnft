import nft from './components/nft.vue';
import mainPage from './components/mainPage.vue';

export default [
    { path: '/', component: mainPage },
    { path: '/nft', component: nft },
]